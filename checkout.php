<!doctype html>
<html>
  <head>
   <meta charset="UTF-8">
   <title>Checkout Mirror Fashion</title>
   <link rel="stylesheet" href="css/bootstrap.css">
   <meta name="viewport" content="width=device-width">
   <style>
     .form-control:invalid {
        border: 1px solid #cc0000;
      }
   </style>
  </head>
  <body>
    <div class="jumbotron">
     <div class="container">
     <h1>Ótima escolha!</h1>
     <p>Obrigado por comprar na Mirror Fashion Preencha seus dados para efetivar a compra.</p>
    </div>
   </div>
 
 <div class="container">
  <div class="row">
  <div class="col-sm-4">
  <div class="panel panel-default">
   <div class="panel-heading"><h2>Sua compra</h2></div>
    <div class="panel-body panel-success">
     <img src="img/produtos/foto<?=$_POST["id"]?>-<?=$_POST["cor"]?>.png" alt="<?=$_POST["nome"]?>" class="img-thumbnail img-responsive hidden-xs">
     <dl>
       <dt>Cor</dt>
       <dd><?=$_POST["cor"]?></dd>
       <dt>Tamanho</dt>
       <dd><?=$_POST["tamanho"]?></dd>
       <dt>Produto</dt>
       <dd><?=$_POST["nome"]?></dd>
       <dt>Preço</dt>
       <dd><?=$_POST["preco"]?></dd>
       <dt>Id</dt>
       <dd><?=$_POST["id"]?></dd>
    </dl>
   </div>
   </div>
   </div>

   <form class="col-sm-8 col-lg-9">
    <div class="row">
    <fieldset class="col-md-6">
     <legend>Dados Pessoais</legend>
      
      <div class="form-group">
       <label for="nome">Nome completo</label>
       <input type="text" class="form-control" id="nome" name="nome" autofocus required>
      </div>
      
      <div class="form-group">
       <label for="email">Email</label>
       <span class="input-group-addon">@</span>
       <input type="email" class="form-control" id="email" name="email" placeholder="email@exemplo.com">
      </div>

      <div class="form-group">
       <label for="cpf">CPF</label>
       <input type="text" class="form-control" id="cpf" name="cpf" placeholder="000.000.000-00" required>
      </div>

      <div class="form-group">
       <label for="cep">CEP</label>
       <input type="text" class="form-control" id="cep" name="cep" placeholder="00000-000">
      </div>

      <div class=checkbox">
       <label><input type="checkbox" value="sim" name="span" checked></label>
       Quero receber spam da Mirror Fashion
      </div>

    </fieldset>
    <fieldset class="col-md-6">
     <legend>Cartão de Crédito</legend>

      <div class="form-group">
       <label for="numero-cartao">Numero - CVV</label>
       <input type="text" class="form-control" id="numero-cartao" name="numero-cartao">
      </div>

      <div class="form-group">
       <label for="bandeira-cartao">Bandeira</label>
       <select name="bandeira-cartao" id="bandeira-cartao" class="form-control">
        <option value="master">MasterCard</option>
        <option value="visa">VISA</option>
        <option value="amex">American Express</option>
       <select>
      </div>

      <div class="form-group">
       <label for="validade-cartao">Validade</label>
       <input type="month" class="form-control" id="validade-cartao" name="validade-cartao">
      </div>

    </fieldset>
    <button type="submit" class="btn btn-primary btn-lg pull-right">
     <span class="glyphicon glyphcon-thumbs-up"></span>
     Confirmar Pedido
    </button>
   </div>
   </form>


  </div>
 </div>
</body>
</html>              
