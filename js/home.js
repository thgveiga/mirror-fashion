var banners = ["img/destaque-home.png" , "img/destaque-home-2.png"];
var bannerAtual = 0;
var playOrPause = 'play';
var timer;

$('.novidades').addClass('painel-compacto');
$('.novidades button').click(function(){
  $('.novidades').removeClass('painel-compacto');
});

function trocaBanner(){
 //bannerAtual = (bannerAtual + 1) %2;
 //bannerAtual++;
 bannerAtual = (bannerAtual === banners.length) ? 0 : bannerAtual;
 document.querySelector('.destaque img').src = banners[bannerAtual++];
}

function play(){
 playOrPause = 'play';
 timer = setInterval(function(){trocaBanner();}, 4000);
}

function pause(){
 playOrPause = 'pause';
 clearInterval(timer);
}

document.querySelector('#form-busca').onsubmit = function(){
  if(document.querySelector('#q').value == ''){
    //alert("nao podia ter deixado em branco a busca");
    document.querySelector('#form-busca').style.background = 'red';
    return false;
  }
};

document.querySelector('.destaque img').onclick = function(){
if(playOrPause === 'play'){
   pause();
}else{
   play();
 }
}

play();

