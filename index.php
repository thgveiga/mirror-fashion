<!DOCTYPE html>
<html>
  <head>
    <title>Mirror Fashion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/mobile.css" media="(max-width: 939px)">
  </head>
  <body>


     <?php include("_cabecalho.php");?>
     <div class="container destaque">
	     <section class=busca>
	     <h2>Busca</h2> 
	     <form action="http://www.google.com.br/search" id="form-busca">
	       <input type="search" name="q" id="q" placeholder="uma busca no google">
	       <input type="image" src=img/busca.png>
	     </form>
	     </section>

	     <section class="menu-departamentos">
	     <h2>Departamentos</h2>

	     <nav>
		<ul>
		  <li>
                    <a href="#">blusas e camisas</a>
                    <ul>
                       <li><a href="#">Manga Curta</a></li>
                       <li><a href="#">Manga Comprida</a></li>
                       <li><a href="#">Camisa social</a></li>
                       <li><a href="#">Camisa Casual</a></li>
                    </ul> 
                  </li>
		  <li><a href="#">calças</a></li>
		  <li><a href="#">saias</a></li>
		  <li><a href="#">vestidos</a></li>
		  <li><a href="#">sapatos</a></li>
		  <li><a href="#">bolsas e carteiras</a></li>
		  <li><a href="#">acessórios</a></li>
		</ul>
	     </nav>
            </section>

	     <img src="img/destaque-home.png" alt="Promoção: Big City Night">
     </div>
   
     <div class="container paineis">
       <section class="painel novidades">
         <h2>Novidades</h2>
           <ol>
           
      <?php
          $conexao = mysqli_connect("127.0.0.1", "root", "", "WD43");
          $dados = mysqli_query($conexao, "select * from produtos order by data desc limit 12");
          while($produto = mysqli_fetch_array($dados)): 
      ?>
           <li>
               <a href="produto.php?id=<?=$produto["id"]?>" target="_blank">
                 <figure>
                   <img src="img/produtos/miniatura<?=$produto["id"]?>.png">
                   <figcaption><?=$produto["nome"]?> por <?=$produto["preco"]?></figcaption>
                 </figure>
               </a>
             </li>
       <?php
          endwhile;
       ?>
<!--
           <li>
               <a href="produto.html">
                 <figure>
                   <img src="img/produtos/miniatura2.png">
                   <figcaption>Fuzz Cardigan por R$ 129,90</figcaption>
                 </figure>
               </a>
             </li>

           <li>
               <a href="produto.html">
                 <figure>
                   <img src="img/produtos/miniatura3.png">
                   <figcaption>Fuzz Cardigan por R$ 129,90</figcaption>
                 </figure>
               </a>
             </li>

            <li>
               <a href="produto.html">
                 <figure>
                   <img src="img/produtos/miniatura4.png">
                   <figcaption>Fuzz Cardigan por R$ 129,90</figcaption>
                 </figure>
               </a>
             </li>

            <li>
               <a href="produto.html">
                 <figure>
                   <img src="img/produtos/miniatura5.png">
                   <figcaption>Fuzz Cardigan por R$ 129,90</figcaption>
                 </figure>
               </a>
             </li>

             <li>
               <a href="produto.html">
                 <figure>
                   <img src="img/produtos/miniatura6.png">
                   <figcaption>Fuzz Cardigan por R$ 129,90</figcaption>
                 </figure>
               </a>
             </li>
-->
           </ol>
       <button type="button">Mostra mais</button>
       </section>

      <section class="painel mais-vendidos">
         <h2>Mais vendidos</h2>
           <ol>
           
      <?php
         
          $dados2 = mysqli_query($conexao, "select * from produtos order by vendas desc limit 12");
          while($produto2 = mysqli_fetch_array($dados2)): 
      ?>

           <li>
               <a href="produto.php?id=<?=$produto2["id"]?>"  target="_blank">
                 <figure>
                   <img src="img/produtos/miniatura<?=$produto2["id"]?>.png">
                   <figcaption><?=$produto2["nome"]?> por <?=$produto2["preco"]?></figcaption>
                 </figure>
               </a>
             </li>
       <?php
          endwhile;
       ?>
<!--
           <li>
               <a href="produto.html">
                 <figure>
                   <img src="img/produtos/miniatura2.png">
                   <figcaption>Fuzz Cardigan por R$ 129,90</figcaption>
                 </figure>
               </a>
             </li>

           <li>
               <a href="produto.html">
                 <figure>
                   <img src="img/produtos/miniatura3.png">
                   <figcaption>Fuzz Cardigan por R$ 129,90</figcaption>
                 </figure>
               </a>
             </li>

            <li>
               <a href="produto.html">
                 <figure>
                   <img src="img/produtos/miniatura4.png">
                   <figcaption>Fuzz Cardigan por R$ 129,90</figcaption>
                 </figure>
               </a>
             </li>

            <li>
               <a href="produto.html">
                 <figure>
                   <img src="img/produtos/miniatura5.png">
                   <figcaption>Fuzz Cardigan por R$ 129,90</figcaption>
                 </figure>
               </a>
             </li>

             <li>
               <a href="produto.html">
                 <figure>
                   <img src="img/produtos/miniatura6.png">
                   <figcaption>Fuzz Cardigan por R$ 129,90</figcaption>
                 </figure>
               </a>
             </li>
-->
           </ol>
 <button type="button">Mostra mais</button>
       </section>
              
      <?php include("_rodape.php");?>
     </div>
     <script src="js/jquery.js"></script>
     <script src="js/home.js"></script>
  </body>
</html>
